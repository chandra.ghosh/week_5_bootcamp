FROM python:3.6.7-slim


# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /code

COPY . /code/
RUN apt-get update && apt-get install --no-install-recommends
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

RUN pip install --upgrade pip
RUN pip install pipenv
RUN apt install -y libpq-dev python3-dev build-essential

RUN pip install -r /code/requirements.txt
RUN pip install gunicorn
RUN pip install elasticsearch_dsl

EXPOSE 8000

