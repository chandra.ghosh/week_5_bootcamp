from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models

connections.create_connection(hosts=['https://elastic:NtsXzaGQKyxOILrDlIcl7pFG@dc78d08f621e4636b16152742954f92b.ap-southeast-1.aws.found.io:9243'])


class QuestionIndex(DocType):
    user = Text()
    title = Text()
    timestamp = Date()
    content = Text()
    id = Text()
    
    class Index:
        name = 'question-index'


def bulk_indexing():
    es = Elasticsearch()
    for b in models.Question.objects.all():
        b.indexing()


def search(title):
    s = Search().query("query_string", query='*'+title+'*', fields=['title', 'content'])
    response = s.execute()
    id_list = []
    for i in response:
        id_list.append(i['id'])

    return id_list

