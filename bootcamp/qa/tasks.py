from __future__ import absolute_import, unicode_literals
from bootcamp.qa.models import Question
from celery import shared_task
import json
import os


@shared_task
def downloadQuestions(user_id):
    """ import time; time.sleep(10) """
    active_user = user_id
    filename = str(active_user) + '.json'
    filepath = os.path.join(os.environ.get('HOME'), filename)
    questions = Question.objects.filter(user=active_user)
    final_questions_data = dict()

    for question in questions:
        data = {
            "title": question.title,
            "content": question.content,            
        }
        final_questions_data[question.id] = data
    with open(filepath, 'w+') as outfile:
        json.dump(final_questions_data, outfile)
